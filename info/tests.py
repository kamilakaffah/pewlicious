from django.test import TestCase

from .views import info

# Create your tests here.

class UnitTest(TestCase):
    def test_url_ada(self):
        response = self.client.get('/info/')
        self.assertEqual(response.status_code, 200)
    
    def test_html(self):
        response = self.client.get('/info/')
        self.assertTemplateUsed(response, 'info.html')