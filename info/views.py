from django.shortcuts import render, redirect
from .forms import ReviewForm
from .models import Review
from homepage.models import Restaurant
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.

def info(request):
    if request.method == 'GET':
        form = ReviewForm()
    elif request.method == 'POST':
        form = ReviewForm(request.POST)
        # print(request.POST)

        if form.is_valid():
            new_review = Review(
                resto = Restaurant.objects.get(pk = 1),
                name = form.data['name'],
                content = form.data['content'],
            )

            new_review.save()
            form = ReviewForm()

        return HttpResponseRedirect('/info/')
    
    reviews = Review.objects.all()

    resto = Restaurant.objects.get(pk = 1)

    imagefile = resto.image

    context = {
        'form': form,
        'reviews': reviews,
        'imagefile': imagefile,
        'resto': resto,
    }

    return render(request, 'info.html', context)