from django.db import models
from homepage.models import Restaurant

# Create your models here.
class Review(models.Model):
    resto = models.ForeignKey(Restaurant, on_delete = models.CASCADE)
    
    name = models.CharField(max_length = 25, default = '')
    content = models.TextField(default = '')

    def __str__(self):
        return self.name