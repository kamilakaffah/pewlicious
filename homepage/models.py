from django.db import models
from django.utils import timezone

# from django.forms import ModelForm

# Create your models here.
   
class Rest(models.Model):
    
    place   = models.CharField(max_length= 30 ,default="jabodetabek")

    def __str__(self):
        return self.place

class Restaurant(models.Model):
    places  = models.ForeignKey(Rest, on_delete=models.CASCADE)
    nama    = models.CharField(max_length = 30 , default="Random")
    alamat = models.CharField(max_length = 50, default = 'Jl. Depok Kota Depok')
    email = models.EmailField(max_length = 30, default = 'gg@gmail.com')
    desc = models.TextField(default = '')


    likes = 0
    image = models.FileField(upload_to = 'media/', null=True, verbose_name="")

    def __str__(self):
        return self.nama
