from django.urls import path, include
from . import views
from django.conf.urls import url, include
from listresto import urls

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name = "home"),
]