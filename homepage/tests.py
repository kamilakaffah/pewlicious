from django.test import TestCase

# Create your tests here.

from .views import home
from .forms import RestForm

class CalculatorTest(TestCase):
    def test_url_exists(self):
        response    = self.client.get('/');
        self.assertEqual(response.status_code, 200);

    def test_ini_htmlnya_bener_ga(self):
        response    = self.client.get('/');
        self.assertTemplateUsed(response, 'homepage.html');

    def test_ini_base_htmlnya_bener_ga(self):
        response    = self.client.get('/');
        self.assertTemplateUsed(response, 'homepagebase.html');

    def test_ini_ngapain(self):
        self.assertEqual(True,not False);
