from django.db import models
from django import forms
from .models import ActivityModel


import datetime

class ActivityForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(
        attrs = {
            "placeholder": 'Name'
        }
    ))
    date = forms.DateField(input_formats=['%Y-%m-%d'], widget=forms.TextInput(
        attrs = {
            "placeholder": 'YYYY-MM-DD'
        }
    ))
    hour = forms.TimeField(input_formats=['%H:%M'], widget=forms.TimeInput(
        attrs = {
            "placeholder": 'Hour:Minute'
        }
    ))


    class Meta:
        model = ActivityModel
        fields = ("name", "date", "hour")
        labels ={
            "name": "Name",
            "date": "Date",
            "hour": "time"
        }
